let printField = document.getElementById("arvosana");
let inputs = document.querySelectorAll("input");

function laskepisteet() {
    this.Pisteet = 0;
    this.arvosana = undefined;

 
    if (checkInputvalue(inputs)) {
        for (let current of inputs) {
        this.Pisteet += Number(current.value);
        }

        this.arvosana = getArvosana(this.Pisteet);

        printField.innerHTML = `Pistemäärä yhteensä: ${
        this.Pisteet
    }<br>Arvosana: ${this.arvosana}`;
    } else {
        printField.innerHTML = `Pistemäärä yhteensä: VIRHE<br>Arvosana: VIRHE`;
    }
}

     
function checkInputvalue(inputs) {
    for (let current of inputs) {
        if (current.value > 6) {
            return false;
        }
    }
        return true;
}

      
function getArvosana(Pisteet) {
    if (Pisteet <= 12 && Pisteet >= 0) {
        return 0;
    } else if (Pisteet <= 15 && Pisteet >= 13) {
        return 1;
    } else if (Pisteet <= 17 && Pisteet >= 16) {
        return 2;
    } else if (Pisteet <= 19 && Pisteet >= 18) {
        return 3;
    } else if (Pisteet <= 21 && Pisteet >= 20) {
        return 4;
    } else if (Pisteet <= 24 && Pisteet >= 22) {
        return 5;
    } else {
        console.log("Virhe laskiessa arvosanaa");
        return "VIRHE";
    }
}
