var hcpdata = [{ "name": "Ari", "hcp": 54 },
    { "name": "Pasi", "hcp": 5 },
    { "name": "Seppo", "hcp": 20 }
];
let hcplist = document.getElementById("myList");

function showHandicap() {
    if (hcplist.childElementCount < 3){
        for(let obj of hcpdata){
          let liNode = document.createElement("li");
          let task = document.createTextNode(`${obj.name} ${obj.hcp}`);
          liNode.appendChild(task);
          hcplist.appendChild(liNode);
        }
    } else {
        alert("Voidaan tulostaa vain kerran");
    }
}


