// Block.js
function Block(ctx,x,y) {
	this.ctx = ctx;
	this.w = 50;
	this.h = 20;
	this.x = x;
	this.y = y;
	// Eri väristen palikkoitten valitseminen randomisti.
	this.color = "rgb("+Math.floor(Math.random()*256)+","+
						Math.floor(Math.random()*256)+","+
						Math.floor(Math.random()*256)+")";
	
	// Palikan luonti
	this.draw = function() {	
		ctx.fillStyle = this.color;
		ctx.fillRect(this.x,this.y,this.w,this.h);
	}
	
}