function allowDrop(event) {
    event.preventDefault();
  }
  
  function drag(event) {
    event.preventDefault();
  }
  
  function drop(event) {
    event.preventDefault();
    console.log("Dropped");
    let files = event.dataTransfer.files;

    for (let file of files) {
      let fr = new FileReader();
      if (file.type.match(/image/i) != null) {
        fr.onload = e => {
          let image = document.createElement("img");
          image.classList.add("thumbnail");
          image.setAttribute("src", e.target.result);
          event.target.appendChild(image);
        };
      } else {
        alert("Ainoastaan Kuva formaatti käy");
      }
      fr.readAsDataURL(file);
    }
  }
  