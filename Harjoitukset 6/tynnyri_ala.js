const canvas = document.getElementById("app");
      const ctx = canvas.getContext("2d");
      const input = document.getElementById("nesteKorkeus");

      function updateValue() {
        const p = document.getElementById("showValue");
        p.innerHTML = `Korkeus: ${input.value}cm`;
        app();
      }

      function app() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // säde ja pituus
        const radius = 1;
        const length = 1;
        let height = input.value / 100;

        // koko tynnyrin pinta-ala ja tilavuus
        let surfaceArea = Math.PI * radius * radius;
        let maxCapacity = (surfaceArea * length * 1000).toFixed(2);

        // kulman kaava
        let angle = Math.acos((radius - height) / radius);
        console.log("angle = " + angle * (180 / Math.PI) * 2);

        
        const startPoint = Math.PI / 2;
        let startAngle = startPoint - angle;
        let endAngle = startPoint + angle;


        let currentAngle = angle * 2;

        let segment = null;
        let currentCap = null;
        
        // Nesteen tilavuus valitulla korkeudella
        if (height <= 1) {
          segment = (
            ((radius * radius) / 2) *
            (currentAngle - Math.sin(currentAngle)) *
            1000
          )

          currentCap = (segment * length).toFixed(2);
        } else if (height > 1) {
          segment = (
            ((radius * radius) / 2) *
            (2 * Math.PI - (currentAngle - Math.sin(currentAngle))) *
            1000
          )

          currentCap = (maxCapacity - segment * length).toFixed(2);
        }

        console.log("current angle " + currentAngle * (180 / Math.PI));

        ctx.beginPath();
        ctx.fillStyle = "white";
        ctx.font = "22px Arial";
        ctx.fillText(`Tynnyrin pituus: ${length * 100}cm`, 10, 30);
        ctx.fillText(`Tynnyrin halkaisija: ${radius * 2 * 100}cm`, 10, 55);
        ctx.fillText(`Tynnyrin tilavuus: ${maxCapacity} litraa`, 10, 80);
        ctx.fillText(`Nestemäärä: ${currentCap} litraa`, 10, 120);
        ctx.closePath();

        // Piirtää tynnyrin neste määrän
        ctx.beginPath();
        ctx.fillStyle = "blue";
        ctx.arc(500, 220, 200, startAngle, endAngle);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(500, 220, 200, 0, Math.PI * 2);
        ctx.closePath();
        ctx.stroke();
      }
