// Näytetään aluetta kun kartta latautuu
var map = L.map('map').setView([62.244, 25.755], 6);

// Näytetään kartan ala laidassa mitäkarttaa käytetään
var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '© OpenStreetMap'
}).addTo(map);

// Hakee 6 Json tiedostoa
var url1 = 'nahtavyydet.json';  
var url2 = 'nuotiopaikka.json';  
var url3 = 'museo.json'; 
var url4 = 'torni.json'; 
var url5 = 'luontopolku.json'; 
var url6 = 'parkkipaikka.json'; 

// Layer 1
// Asetetaan nähtävyydet markerille väri
function getColor(id){
	return id == '1' ? 'purple' :
						'white';
}	

// Asetetaan markerille tyyli, johon kerrotaan 
function style(feature) {
	return {
		fillColor: setColor(feature.properties.id),
		fillOpacity: 0.5,
		weight: 2,
		opacity: 1,
		color: '#ffffff',
		dashArray: '3'
	};
}
	
var nahtavyydet = L.geoJson(null, {
	pointToLayer: function(feature, latlng) {
		return L.circleMarker(latlng, {
		radius:6,
		opacity: .5,
		color:getColor(feature.properties.id),
		fillColor:  getColor(feature.properties.id),
		fillOpacity: 0.8
		}); 
	},

	onEachFeature: function (feature, layer) {
		layer._leaflet_id = feature.properties.INFO;
		var popupContent = feature.properties.INFO;
		if (feature.properties && feature.properties.popupContent) {
			popupContent += feature.properties.popupContent;
		}
			layer.bindPopup(popupContent);
	}
});

// Hakee tiedot nahtavyydet.Json tiedostosta
$.getJSON(url1, function(data) {			
nahtavyydet.addData(data).addTo(map);
});	

//END Layer1

/// Layer 2

function getColor2(id){
	return id == '2' ? 	'red' :
						'white';
}	

// Set style function that sets fill color property
function style2(feature) {
	return {
		fillColor: setColor(feature.properties.id),
		fillOpacity: 0.5,
		weight: 2,
		opacity: 1,
		color: '#ffffff',
		dashArray: '3'
	};
}	

//layer2
var nuotiopaikka = L.geoJson(null, {
	
	pointToLayer: function(feature, latlng) {
		return L.circleMarker(latlng, {
		radius:6,
		opacity: .5,
		color:getColor2(feature.properties.id),
		fillColor:  getColor2(feature.properties.id),
		fillOpacity: 0.8
		});  
	},
	
	onEachFeature: function (feature, layer) {
		layer._leaflet_id = feature.properties.INFO;
		var popupContent = feature.properties.INFO;
		if (feature.properties && feature.properties.popupContent) {
			popupContent += feature.properties.popupContent;
		}
			layer.bindPopup(popupContent);
	}
});

$.getJSON(url2, function(data2) {			
	nuotiopaikka.addData(data2);
});		
	
//End layer 2		

/// Layer 3
function getColor3(id){
	return id == '3' ? 	'aqua' :
						'white';
}	
// Set style function that sets fill color property
function style3(feature) {
	return {
		fillColor: setColor(feature.properties.id),
		fillOpacity: 0.5,
		weight: 2,
		opacity: 1,
		color: '#ffffff',
		dashArray: '3'
	};
}	

//layer3
var museo = L.geoJson(null, {
	
	pointToLayer: function(feature, latlng) {
		return L.circleMarker(latlng, {
			radius:6,
			opacity: .5,
			color:getColor3(feature.properties.id),
			fillColor:  getColor3(feature.properties.id),
			fillOpacity: 0.8
		});  
	},
	
	onEachFeature: function (feature, layer) {
		layer._leaflet_id = feature.properties.INFO;
		var popupContent = feature.properties.INFO;	
		if (feature.properties && feature.properties.popupContent) {
			popupContent += feature.properties.popupContent;
		}
			layer.bindPopup(popupContent);
	}
});

$.getJSON(url3, function(data3) {			
	museo.addData(data3);
});		
	
//End layer 3

/// Layer 4

function getColor4(id){
	return id == '4' ? 	'orange' :
						'white';
}	

// Set style function that sets fill color property
function style4(feature) {
	return {
		fillColor: setColor(feature.properties.id),
		fillOpacity: 0.5,
		weight: 2,
		opacity: 1,
		color: '#ffffff',
		dashArray: '3'
	};
}	

//layer4
var torni = L.geoJson(null, {
	
	pointToLayer: function(feature, latlng) {
		return L.circleMarker(latlng, {
			radius:6,
			opacity: .5,
			color:getColor4(feature.properties.id),
			fillColor:  getColor4(feature.properties.id),
			fillOpacity: 0.8
		});
	},
	
	onEachFeature: function (feature, layer) {
		layer._leaflet_id = feature.properties.INFO;
		var popupContent = feature.properties.INFO;
		if (feature.properties && feature.properties.popupContent) {
			popupContent += feature.properties.popupContent;
		}
			layer.bindPopup(popupContent);
	}
});

$.getJSON(url4, function(data4) {			
	torni.addData(data4);
});		
	
//End layer 4

/// Layer 5
function getColor5(id){
	return id == '5' ? 	'green' :
						'white';
}	
// Set style function that sets fill color property
function style5(feature) {
	return {
		fillColor: setColor(feature.properties.id),
		fillOpacity: 0.5,
		weight: 2,
		opacity: 1,
		color: '#ffffff',
		dashArray: '3'
	};
}	

//layer5
var luontopolku = L.geoJson(null, {
	pointToLayer: function(feature, latlng) {
		return L.circleMarker(latlng, {
			radius:6,
			opacity: .5,
			color:getColor5(feature.properties.id),
			fillColor:  getColor5(feature.properties.id),
			fillOpacity: 0.8
		});
	},

	onEachFeature: function (feature, layer) {
		layer._leaflet_id = feature.properties.INFO;
		var popupContent = feature.properties.INFO;
		if (feature.properties && feature.properties.popupContent) {
			popupContent += feature.properties.popupContent;
		}
			layer.bindPopup(popupContent);
	}
});

$.getJSON(url5, function(data5) {			
	luontopolku.addData(data5);
});		
	
//End layer 5

/// Layer 6

function getColor6(id){
	return id == '6' ? 	'blue' :
						'white';
}	

// Set style function that sets fill color property
function style6(feature) {
	return {
		fillColor: setColor(feature.properties.id),
		fillOpacity: 0.5,
		weight: 2,
		opacity: 1,
		color: '#ffffff',
		dashArray: '3'
	};
}	

//layer6

var parkkipaikka = L.geoJson(null, {
	
	pointToLayer: function(feature, latlng) {
		return L.circleMarker(latlng, {
			radius:6,
			opacity: .5,
			color:getColor6(feature.properties.id),
			fillColor:  getColor6(feature.properties.id),
			fillOpacity: 0.8
		});
	},
	
	onEachFeature: function (feature, layer) {
		layer._leaflet_id = feature.properties.INFO;
		var popupContent = feature.properties.INFO;
		if (feature.properties && feature.properties.popupContent) {
			popupContent += feature.properties.popupContent;
		}
			layer.bindPopup(popupContent);
	}
});

$.getJSON(url6, function(data6) {			
	parkkipaikka.addData(data6);
});		
	
//End layer 6

///////////////////// Layer Control  /////////////////////////////////////////////////
// Katta sovellus käyttää OpenStreetMap kartta pohjaa
var baseMaps = {
	'Open StreetMap': osm
};
// Voi valita mitkä markerit näkyvät kartaalla.
var overlayMaps = {
	'<i class="fa-solid fa-person-hiking"></i> Luontopolkku': luontopolku,
	'<i class="fa-solid fa-square-parking"></i> Parkkipaikka': parkkipaikka,
	'<i class="fa-solid fa-fire"></i> Nuotiopaikka': nuotiopaikka,
	'<i class="fa-solid fa-landmark"></i> Museo': museo,
	'<i class="fa-solid fa-tower-observation"></i> Näkötorni': torni,
	'<i class="fa-solid fa-camera"></i> Nahtavyydet': nahtavyydet,
};
// Asettaa layerin näkyväksi kartalle
L.control.layers(baseMaps, overlayMaps).addTo(map);
