var mymap = L.map('mapid2').setView([62.244, 25.755], 6);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', 
    {
        attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
        tileSize: 512,
        maxZoom: 18,
        zoomOffset: -1,
        id: 'mapbox/streets-v11',
        accessToken: 'pk.eyJ1Ijoiam9wcGUxOTgiLCJhIjoiY2w1OWNoZ3pmMDM1bTNrbzFheGo2YmhpciJ9.Jq9JfzRUq210NERYVzJj2w'
    }
).addTo(mymap);

// Tarkistaa JSON tiedoston data tiedosta minkä tyyppinen kenttä on kyseessä ja palauttaa kentälle oikean värisen markerin.
function checkForMarker(data) {
    const CustomMarker = L.Icon.extend({
      options: {
        iconSize: [32, 32]
      }
    });

    let greenMarker = new CustomMarker({iconUrl: 'icons8-place-marker-green.png'}),
        yellowMarker = new CustomMarker({iconUrl: 'icons8-place-marker-yellow.png'}),
        blueMarker = new CustomMarker({iconUrl: 'icons8-place-marker-blue.png'}),
        redMarker = new CustomMarker({iconUrl: 'icons8-place-marker-red.png'});

    switch (data.Tyyppi) {
        case "Kulta": {
            return yellowMarker;
           
        }
    
        case "Kulta/Etu": {
            return blueMarker;
        }
    
        case "Etu": {
            return greenMarker;
            
        }
        
        case "?": {
            return redMarker;
           
        }
        default:
            return redMarker;
           
    }
}


$.ajax({
    url: "kentat.json"
    })
    .fail(() => {
      console.log("Error with ajax");
    })
    .done(data => {
      console.log(`Done with file`);
      $.each(data.kentat, (index, data) => {
        let content = `
        <h2>${data.Kentta}</h2>
        ${data.Kuvaus}<br><br>
        Osoite: ${data.Osoite}<br>
        Puhelin: ${data.Puhelin}<br>
        Sähköposti: ${data.Sahkoposti}<br>
        Web: <a href="${data.Webbi}" target="blank">${data.Webbi}</a>
        `;
        let latlng = L.latLng(data.lat, data.lng);
        let popup = L.popup("kentat").setContent(content);
        let marker = L.marker([data.lat, data.lng], {
          icon: checkForMarker(data)
        })
          .bindPopup(popup)
          .addTo(mymap);
      });
    });
