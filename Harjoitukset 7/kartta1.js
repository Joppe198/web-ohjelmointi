var mymap = L.map('mapid').setView([62.244, 25.755], 14);


L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
tileSize: 512,
maxZoom: 18,
zoomOffset: -1,
id: 'mapbox/streets-v11',
accessToken: 'pk.eyJ1Ijoiam9wcGUxOTgiLCJhIjoiY2w1OWNoZ3pmMDM1bTNrbzFheGo2YmhpciJ9.Jq9JfzRUq210NERYVzJj2w'
}).addTo(mymap);

var marker = L.marker([62.244, 25.755]).addTo(mymap);


var circle = L.circle([62.244348661369465, 25.740022659301758], {
    color: 'yellow',
    fillColor: 'yellow',
    fillOpacity: 0.5,
    radius: 450
}).addTo(mymap);

var polygon = L.polygon([
    [62.237972531969135, 25.75223207473755],
    [62.239341814488185, 25.75523614883423],
    [62.239081955432034, 25.756051540374756],
    [62.237452788021066, 25.753369331359863]
]).addTo(mymap);

marker.bindPopup("<b>Hello world!</b><br>Jyväskylän keskusta").openPopup();
circle.bindPopup("Harjun EK.");
polygon.bindPopup("MM-ralli-Kilpailukeskus");



var popup = L.popup();
/*
function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(mymap);
}

mymap.on('click', onMapClick);
*/
