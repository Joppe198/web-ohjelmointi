// setup 
const labels = ['Ford', 'Opel', 'Toyota'];
const data = {
    labels: labels,
    datasets: [{
      label: 'Autojen Lukumäärä',
      data: [0, 0, 0],
      backgroundColor: [
        'rgba(255, 26, 104, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)'
      ],
      borderColor: [
        'rgba(255, 26, 104, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 1
    }]
  };

  // config 
  const config = {
    type: 'bar',
    data,
    options: {
      scales: {
        y: {
          beginAtZero: true,
          ticks: {min: 0, stepSize: 1}
        }
      },

    }
  };

  // render init block
  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

  function updatecarValue(labeltext) {
    const indexnum = labels.indexOf(labeltext);
    myChart.config.data.datasets[0].data[indexnum] ++;
    myChart.update();
  }

  function nollaa() {
    myChart.config.data.datasets[0].data[0] = 0;
    myChart.config.data.datasets[0].data[1] = 0;
    myChart.config.data.datasets[0].data[2] = 0;
    myChart.update();
  }