class Yhteenlaskin extends React.Component {
    constructor(props) {
      super(props);
      this.state = { summa: "" };
      this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
      let sum = parseInt(this.refs.n1.value) + parseInt(this.refs.n2.value);
      this.setState({ summa: sum });
    }

    render() {
      return (
        <div>
            <h2>Summa laskin</h2>
          <table>
            <tbody>
              <tr>
                <td>Luku1:</td>
                <td>
                  <input type="text" ref="n1" placeholder="Anna luku" onChange={this.handleChange}/>
                </td>
              </tr>
              <tr>
                <td>Luku2:</td>
                <td>
                  <input
                    type="text" ref="n2" placeholder="Anna luku" onChange={this.handleChange} />
                </td>
              </tr>
              <tr>
                <td>Tulos:</td>
                <td>
                  <input type="text" ref="tulos" placeholder="Summa on" value={this.state.summa} />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      );
    }
  }

  ReactDOM.render(<Yhteenlaskin />, document.getElementById("ratkaisu3"));
