class FromArrayToObject extends React.Component {
    render() {
      let names = ["Arska", "Basso", "Mixu"];
      let nameList = names.map((name, index) => {
        return <p key={index}>Terve {name}!</p>;
      });
      return nameList;
    }
  }

  ReactDOM.render(
    <FromArrayToObject />,
    document.getElementById("root")
  );
