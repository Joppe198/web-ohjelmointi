// Hakee luvut 
function haeNumero(tunnus) {
    return parseInt(document.getElementById(tunnus).value);
}
// Tulostaa summa laskun
function tulostaSumma(tulos) {
    document.getElementById("tulos").innerHTML = tulos;
}
// Laskee summa laskun
function laskeSumma() {
    tulostaSumma(haeNumero("n1") + haeNumero("n2"));
}