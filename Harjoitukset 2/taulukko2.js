const taulukkoArray = [11, 22, 33, 44];

let printArray = function(index) {
    return `taulukkoArray[${index}] = ${taulukkoArray[index]}<br>`;
};


let arrayAvg = function() {
    return arraySum() / arrayCount();
}

let arraySum = function() {
    let s = 0;
    for (let sum of taulukkoArray) {
        s += sum;
    }
    return s;
}

let arrayCount = function() {
    let c = 0;
    for (let i = 0; i < taulukkoArray.length; i++) {
        c++;
    }
    return c;
}

function writeResults() {
    for (let i = 0; i < taulukkoArray.length; i++) {
        document.getElementById("results").innerHTML += printArray(i);
    }
    document.getElementById("results").innerHTML += `Lukumäärä: ${arrayCount()}<br>`;
    document.getElementById("results").innerHTML += `Summa: ${arraySum()}<br>`;
    document.getElementById("results").innerHTML += `Keskiarvo: ${arrayAvg()}<br>`;

}
window.onload = writeResults;