class Vauhtilaskin {
    constructor(h, m, s, km) {
        this.h = Number(h);
        this.m = Number(m);
        this.s = Number(s);
        this.km = Number(km);
    }

    get kmhpace() {
        return this.calcKmhPace;
    }

    get minkmpace() {
        return this.calckMinkmPace;
    }

    calcKmhPace() {
        let tunnit = parseInt(this.h) + (parseInt(this.m) * 60 + parseInt(this.s)) / 3600;
        return (this.km / tunnit).toFixed(2);
    }
    calckMinkmPace() {
        let sekunnit = parseInt(this.h) * 60 * 60 + parseInt(this.m) * 60 + parseInt(this.s);
        let minuutit = 0;

        while (sekunnit >= 60) {
            minuutit++;
            sekunnit -= 60;
        }
        let sekunninkymmenykset = sekunnit / 60;
        return ((minuutit + sekunninkymmenykset) / this.km).toFixed(2);
    }
}
let tunnit = document.getElementById("tunnit");
let minuutit = document.getElementById("minuutit");
let sekunnit = document.getElementById("sekunnit");
let kilometrit = document.getElementById("kilometrit");

tunnit.defaultValue = 1;
minuutit.defaultValue = 13;
sekunnit.defaultValue = 13;
kilometrit.defaultValue = 20;

const laskin = new Vauhtilaskin(
    tunnit.value,
    minuutit.value,
    sekunnit.value,
    kilometrit.value
);

const päivitä = () => {
    laskin.h = tunnit.value;
    laskin.m = minuutit.value;
    laskin.s = sekunnit.value;
    laskin.km = kilometrit.value;
    print();
};

const print = () => {
    document.getElementById("results2").innerHTML = `${laskin.kmhpace()} km/h<br>${laskin.minkmpace()} min/km`;
};
print();