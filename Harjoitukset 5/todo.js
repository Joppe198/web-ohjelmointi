$(document).ready(function () {
    
    $(".add-items").submit(function(event){
      event.preventDefault();
      var item = $("#todo-list-item").val();
      if(item){
    
          $("#list-items").append("<li>" + item + "<a class='remove'>x</a><hr></li>");
        $("#todo-list-item").val("");
      }
    });
    
    $(document).on("click",".remove",function(){
      $(this).parent().fadeOut( "slow", function() {
            $(this).remove();
            localStorage.setItem("listItems",$("#list-items").html());
    });

  });
});
