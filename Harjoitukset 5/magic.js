/* Houses- JavaScript */
$(document).ready(function(){
    // use ajax() to load magic.json file
    // call showHouses from done()-function
    $.ajax({
        url: "magic.json",
        cache: false
    })
    .done(data => {
        console.log("Done");
        showHouses(data);
      })
      .fail((xhr, status, error) => {
        console.log("Error status " + status);
        console.log("Error type " + error);
        console.dir(xhr);
      })
      .always(() => {
        console.log("Ajax function complete");
      });
});

// function shows houses information
function showHouses(data) {
    // loop through all houses data 
    $.each(data.talot, function(index, talo) {
        // create a div element and add "houseContainer" class to it
        var div = $("<div></div>").addClass("houseContainer");
        // create img element and use "houseImage" class to it and src to house image
        var image = $(`<img></img>`)
            .attr("src", talo.image)
            .addClass("houseImage");
        // create p element, use väri as a text and "header" class
        var header = $("<p></p>")
        .addClass("header")
        .text(talo.väri);
        // create p element ja use magic mana_hinta as a text
        var mana_hinta = $("<p></p>")
        .text(talo.mana_hinta);
        // create p element and use magic kortti_tyyppi as a text 
        var kortti_tyyppi = $("<p></p>")
        .text(talo.kortti_tyyppi);
        // create p element and use magic kuvaus as a text as a text and "text" class
        var kuvaus = $("<p></p>")
        .addClass("text")
        .text(talo.kuvaus);
        // create p element and use magic kyky as a text
        var kyky = $("<p></p>")
        .text(talo.kyky);
        // create p element and use magic hinta as a text
        var hinta = $("<p></p>")
        .text(talo.hinta);
        //  add all elements to houseDiv lisÃ¤tÃ¤Ã¤n kaikki luodut elementit taloDiv-elementtiin
        div.append(image, header, mana_hinta, kortti_tyyppi, kuvaus, kyky, hinta);
        // add div to #houses in DOM
        $("#houses").append(div);
    });
}